#!/bin/bash

set -euo pipefail

OUT_DIR=build/bin

mkdir -p $OUT_DIR
rm -rf $OUT_DIR/*

# Convert the tileset.
./scripts/convert-image.py -t 16x16 -P $OUT_DIR/TILESP.BIN --use-true-black assets/megabot/tileset.png $OUT_DIR/TILES.BIN

# And the background tileset.
./scripts/convert-image.py -t 16x16 -p $OUT_DIR/TILESP.BIN --extend-palette --use-true-black assets/megabot/bg.png $OUT_DIR/BGTILES.BIN

# Convert all the sprites.
echo -ne '\0\0' >> $OUT_DIR/SPRITE.BIN
echo -ne '\0\0' >> $OUT_DIR/SPRITEP.BIN
SPRITES_32X32="player enemy explosion"
for sprite in $SPRITES_32X32; do
	./scripts/convert-image.py -t 32x32 -p $OUT_DIR/SPRITEP.BIN --extend-palette --use-true-black --append assets/megabot/$sprite.png $OUT_DIR/SPRITE.BIN
done
./scripts/convert-image.py -t 8x8 -p $OUT_DIR/SPRITEP.BIN --extend-palette --use-true-black --append assets/megabot/shot.png $OUT_DIR/SPRITE.BIN

# Convert the tilemap.
if [ ! -e build/maps/map.json -o ! -e build/maps/bg.json ]; then
	./build-maps.sh
fi
./scripts/convert-tilemap.py build/maps/map.json $OUT_DIR/MAP.BIN -p 2
./scripts/generate-collision-data.py build/maps/map.json $OUT_DIR/MAPC.BIN
./scripts/convert-tilemap.py build/maps/bg.json $OUT_DIR/BG.BIN -p 2

cl65 -t cx16 -l ltcommander.list -o $OUT_DIR/LTCOMMANDER.PRG game.asm

