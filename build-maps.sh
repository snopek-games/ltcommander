#!/bin/bash

set -euo pipefail

TILED=${TILED:-tiled}

OUT_DIR=build/maps
mkdir -p build/maps

$TILED --export-map json maps/map.tmx $OUT_DIR/map.json
$TILED --export-map json maps/bg.tmx $OUT_DIR/bg.json

