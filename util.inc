.ifndef UTIL_INC
.define UTIL_INC

.macro bbr bit_flag, source, addr
	.if bit_flag=1
		bbr0 source, addr
	.elseif bit_flag=2
		bbr1 source, addr
	.elseif bit_flag=4
		bbr2 source, addr
	.elseif bit_flag=8
		bbr3 source, addr
	.elseif bit_flag=16
		bbr4 source, addr
	.elseif bit_flag=32
		bbr5 source, addr
	.elseif bit_flag=64
		bbr6 source, addr
	.elseif bit_flag=128
		bbr7 source, addr
	.endif
.endmacro

.macro bbs bit_flag, source, addr
	.if bit_flag=1
		bbs0 source, addr
	.elseif bit_flag=2
		bbs1 source, addr
	.elseif bit_flag=4
		bbs2 source, addr
	.elseif bit_flag=8
		bbs3 source, addr
	.elseif bit_flag=16
		bbs4 source, addr
	.elseif bit_flag=32
		bbs5 source, addr
	.elseif bit_flag=64
		bbs6 source, addr
	.elseif bit_flag=128
		bbs7 source, addr
	.endif
.endmacro

.macro smb bit_flag, addr
	.if bit_flag=1
		smb0 addr
	.elseif bit_flag=2
		smb1 addr
	.elseif bit_flag=4
		smb2 addr
	.elseif bit_flag=8
		smb3 addr
	.elseif bit_flag=16
		smb4 addr
	.elseif bit_flag=32
		smb5 addr
	.elseif bit_flag=64
		smb6 addr
	.elseif bit_flag=128
		smb7 addr
	.endif
.endmacro

.macro rmb bit_flag, addr
	.if bit_flag=1
		rmb0 addr
	.elseif bit_flag=2
		rmb1 addr
	.elseif bit_flag=4
		rmb2 addr
	.elseif bit_flag=8
		rmb3 addr
	.elseif bit_flag=16
		rmb4 addr
	.elseif bit_flag=32
		rmb5 addr
	.elseif bit_flag=64
		rmb6 addr
	.elseif bit_flag=128
		rmb7 addr
	.endif
.endmacro

.endif
