.org $080d
.segment "STARTUP"
.segment "INIT"
.segment "ONCE"
.segment "CODE"

	jmp start

.include "x16.inc"
.include "util.inc"

; PETSCII
Q_CHAR = $51

; VERA
VSYNC_BIT = $01
DISPLAY_SCALE = 48	; 2.5x scale

; VRAM addresses
VRAM_background_tiles = $07000
VRAM_foreground_tiles = $08000
VRAM_layer0_map = $04000
VRAM_layer1_map = $05000
VRAM_sprites = $0B000

TILES_PALETTE_OFFSET = 2
VRAM_palette_2 = VRAM_palette + (32 * TILES_PALETTE_OFFSET)
SPRITE_PALETTE_OFFSET = 3
VRAM_palette_3 = VRAM_palette + (32 * SPRITE_PALETTE_OFFSET)

VRAM_SPRITE1_sprattr = VRAM_sprattr + (8 * 1)

; Player and MOB constants

.enum MobType
	NONE
	PLAYER
	SHOT
	ENEMY
	EXPLOSION
.endenum

FACING_LEFT_FLAG = $01

.enum PlayerState
	FACING_LEFT = $01
	AIRBORNE	= $02
	MOVING		= $04
	JUMPING		= $08
	SHOOTING	= $10
.endenum

.enum InputState
	DPAD_RIGHT	  = $01
	DPAD_LEFT	  = $02
	DPAD_DOWN	  = $04
	DPAD_UP		  = $08
	BUTTON_START  = $10
	BUTTON_SELECT = $20
	BUTTON_B	  = $40
	BUTTON_A	  = $80
.endenum

.enum CollisionState
	COLLIDE_RIGHT = $01
	COLLIDE_LEFT  = $02
	COLLIDE_DOWN  = $04
	COLLIDE_UP    = $08
.endenum

PLAYER_START_X = 48
PLAYER_START_Y = 112
PLAYER_CENTER_X = 104

PLAYER_SPRITE_RUN_VRAM = VRAM_sprites
PLAYER_SPRITE_RUN_FRAMES = 4
PLAYER_SPRITE_RUN_SHOOT_VRAM = PLAYER_SPRITE_RUN_VRAM + (PLAYER_SPRITE_RUN_FRAMES * 512)
PLAYER_SPRITE_RUN_SHOOT_FRAMES = 4
PLAYER_SPRITE_IDLE_VRAM = PLAYER_SPRITE_RUN_SHOOT_VRAM + (PLAYER_SPRITE_RUN_SHOOT_FRAMES * 512)
PLAYER_SPRITE_SHOOT_VRAM = PLAYER_SPRITE_IDLE_VRAM + 512
PLAYER_SPRITE_JUMP_VROM = PLAYER_SPRITE_SHOOT_VRAM + 512

; These are signed fp (Q4.4) numbers, so (-8 to +7, with 16 fractional values)
PLAYER_VECTOR_LEFT	= <(-1 << 4)
PLAYER_VECTOR_RIGHT = <(1  << 4)
PLAYER_VECTOR_JUMP	= <(-4 << 4)

PLAYER_SHOT_OFFSET_RIGHT_X = 27 << 4
PLAYER_SHOT_OFFSET_RIGHT_Y = 14 << 4
PLAYER_SHOT_OFFSET_LEFT_X = -3 << 4;
PLAYER_SHOT_OFFSET_LEFT_Y = 14 << 4;

PLAYER_SHOT_JIFFIES = 18

SHOT_VECTOR_RIGHT = <(3 << 4)
SHOT_VECTOR_LEFT  = <(-3 << 4)
SHOT_LIFETIME_JIFFIES = 80

GRAVITY_VECTOR_Y = $03
GRAVITY_TERMINAL_Y = $40

MAP_WIDTH = 64
SCREEN_TILE_WIDTH = 15
CAMERA_X_LIMIT = ((MAP_WIDTH - SCREEN_TILE_WIDTH) * 16)

;
; Zero Page
;

input_state      = $34 ; 1 byte
last_input_state = $35 ; 1 byte
vsync_complete   = $36 ; 1 byte
vsync_temp       = $37 ; 2 bytes
mob_current      = $39 ; 1 byte
collision_state  = $3a ; 1 byte

; skipped 5 bytes

collision_point_A1_x = $40 ; 2 bytes
collision_point_A1_y = $42 ; 2 bytes
collision_point_A2_x = $44 ; 2 bytes
collision_point_A2_y = $46 ; 2 bytes
collision_point_B1_x = $48 ; 2 bytes
collision_point_B1_y = $4a ; 2 bytes
collision_point_B2_x = $4c ; 2 bytes
collision_point_B2_y = $4e ; 2 bytes

MOB_MAX = 16
mob_base  = $50
mob_type  = mob_base           ; $50-5f
mob_state = mob_type + MOB_MAX ; $60-6f

; MOB 0 is always the player, and SHOT_MAX worth of the next ids are always shots.
SHOT_START = 1
SHOT_MAX = 4
COMMON_MOB_START = SHOT_START + SHOT_MAX

;
; Variables
;

default_irq_vector: .addr 0

camera_x: .word 0
camera_y: .word 0

; Signed 8-bit fp, meant to be added to unsigned 16-bit fp.
mob_vector_x: .res MOB_MAX
mob_vector_y: .res MOB_MAX
; Unsigned 16-bit fp.
mob_position_x_l: .res MOB_MAX
mob_position_x_h: .res MOB_MAX
mob_position_y_l: .res MOB_MAX
mob_position_y_h: .res MOB_MAX
mob_screen_x_l: .res MOB_MAX
mob_screen_x_h: .res MOB_MAX
mob_screen_y_l: .res MOB_MAX
mob_screen_y_h: .res MOB_MAX
mob_animation: .res MOB_MAX
mob_frame: .res MOB_MAX
mob_frame_jiffies_remaining: .res MOB_MAX
mob_data1: .res MOB_MAX
mob_data2: .res MOB_MAX
mob_data3: .res MOB_MAX
mob_data4: .res MOB_MAX

tiles_palette_fn: .byte "tilesp.bin"
end_tiles_palette_fn:

bg_tiles_fn: .byte "bgtiles.bin"
end_bg_tiles_fn:

bg_fn: .byte "bg.bin"
end_bg_fn:

fg_tiles_fn: .byte "tiles.bin"
end_fg_tiles_fn:

map_fn: .byte "map.bin"
end_map_fn:

mapc_fn: .byte "mapc.bin"
end_mapc_fn:

sprite_palette_fn: .byte "spritep.bin"
end_sprite_palette_fn:

sprite_fn: .byte "sprite.bin"
end_sprite_fn:

;
; Look-up tables
;

.enum Animation
	PLAYER_RUN
	PLAYER_RUN_SHOOT
	PLAYER_IDLE
	PLAYER_SHOOT
	PLAYER_JUMP
	PLAYER_EMPTY
	ENEMY
	EXPLOSION
	SHOT
.endenum

sprite_frame_lut:
	.byte $00, $10, $20, $30

sprite_animation_base_h_lut:
	.byte $05, $05, $06, $06, $06, $06, $06, $06, $06
sprite_animation_base_l_lut:
	.byte $80, $C0, $00, $10, $20, $30, $40, $60, $90
sprite_animation_frames_lut:
	.byte	4,   4,   1,   1,	1,   1,   2,   3,   1
sprite_animation_frame_jiffies_lut:
	.byte	6,   6,   0,   0,	0,   0,   6,   6,   0
sprite_animation_config_lut:
	.byte $A3, $A3, $A3, $A3, $A3, $A3, $A3, $A3, $03

mob_collision_right_h_lut:
	.byte 0, $01, 0, 0, 0
mob_collision_right_l_lut:
	.byte 0, $60, 0, 0, 0

mob_collision_left_h_lut:
	.byte 0, $00, 0, 0, 0
mob_collision_left_l_lut:
	.byte 0, $A0, 0, 0, 0

mob_collision_top_h_lut:
	.byte 0, $00, 0, 0, 0
mob_collision_top_l_lut:
	.byte 0, $A0, 0, 0, 0

mob_collision_bottom_h_lut:
	.byte 0, $01, 0, 0, 0
mob_collision_bottom_l_lut:
	.byte 0, $f0, 0, 0, 0

;
; Program start
;

start:
	lda #$71 ; Enable sprites, layer 0/1 and VGA video
	sta VERA_dc_video

	lda #DISPLAY_SCALE
	sta VERA_dc_hscale
	sta VERA_dc_vscale

	;
	; Confiure Layer 0
	;

	lda #02 ; 32x32 map, with 4bpp
	sta VERA_L0_config

	lda #(VRAM_layer0_map >> 9)
	sta VERA_L0_mapbase
	lda #((VRAM_background_tiles >> 9) | $03) ; 16x16 tiles
	sta VERA_L0_tilebase

	stz VERA_L1_hscroll_l
	stz VERA_L1_hscroll_h
	stz VERA_L1_vscroll_l
	stz VERA_L1_hscroll_h

	;
	; Configure Layer 1
	;

	lda #$12 ; 32x64 map, with 4bpp
	sta VERA_L1_config

	lda #(VRAM_layer1_map >> 9)
	sta VERA_L1_mapbase
	lda #((VRAM_foreground_tiles >> 9) | $03) ; 16x16 tiles
	sta VERA_L1_tilebase

	stz VERA_L1_hscroll_l
	stz VERA_L1_hscroll_h
	stz VERA_L1_vscroll_l
	stz VERA_L1_hscroll_h

	; Load map collision data
	LOAD_FILE mapc_fn, (end_mapc_fn-mapc_fn), map_collision_data

	; Load palette, tiles, and map.
	VERA_LOAD_FILE tiles_palette_fn, (end_tiles_palette_fn-tiles_palette_fn), VRAM_palette_2
	VERA_LOAD_FILE bg_tiles_fn, (end_bg_tiles_fn-bg_tiles_fn), VRAM_background_tiles
	VERA_LOAD_FILE bg_fn, (end_bg_fn-bg_fn), VRAM_layer0_map
	VERA_LOAD_FILE fg_tiles_fn, (end_fg_tiles_fn-fg_tiles_fn), VRAM_foreground_tiles
	VERA_LOAD_FILE map_fn, (end_map_fn-map_fn), VRAM_layer1_map

	; Load sprite data
	VERA_LOAD_FILE sprite_palette_fn, (end_sprite_palette_fn-sprite_palette_fn), VRAM_palette_3
	VERA_LOAD_FILE sprite_fn, (end_sprite_fn-sprite_fn), VRAM_sprites

	; Clear input states
	stz input_state
	stz last_input_state

	; Initialize camera
	stz camera_x
	stz camera_x+1
	stz camera_y
	stz camera_y+1

	; Clear out mob data
	ldx #MOB_MAX
@clear_mob_loop:
	jsr mob_clear
	dex
	bne @clear_mob_loop

	; Initialize the player mob
	lda #MobType::PLAYER
	sta mob_type
	lda #<(PLAYER_START_X << 4)
	sta mob_position_x_l
	lda #>(PLAYER_START_X << 4)
	sta mob_position_x_h
	lda #<(PLAYER_START_Y << 4)
	sta mob_position_y_l
	lda #>(PLAYER_START_Y << 4)
	sta mob_position_y_h
	lda #PLAYER_START_X
	sta mob_screen_x_l
	lda #PLAYER_START_Y
	sta mob_screen_y_l
	lda #Animation::PLAYER_IDLE
	sta mob_animation

	; Initialize the player sprite
	VERA_SET_ADDR VRAM_SPRITE1_sprattr, 1
	lda #<(PLAYER_SPRITE_IDLE_VRAM >> 5)
	sta VERA_data0
	lda #>(PLAYER_SPRITE_IDLE_VRAM >> 5)
	sta VERA_data0
	lda #PLAYER_START_X
	sta VERA_data0
	stz VERA_data0
	lda #PLAYER_START_Y
	sta VERA_data0
	stz VERA_data0
	lda #$0C
	sta VERA_data0
	lda #($A0 | SPRITE_PALETTE_OFFSET)
	sta VERA_data0

	; Clear our internal flag to wait for vsync.
	stz vsync_complete

	; Replace the IRQ handler
	lda IRQVec
	sta default_irq_vector
	lda IRQVec+1
	sta default_irq_vector+1

	sei
	lda #<custom_irq_handler
	sta IRQVec
	lda #>custom_irq_handler
	sta IRQVec+1
	lda #VSYNC_BIT
	sta VERA_ien
	cli
@main_loop:
	wai
	jsr GETIN
	cmp #Q_CHAR
	beq @exit

	jsr capture_input

	lda vsync_complete
	beq @main_loop

	stz vsync_complete

	;
	; Start Process
	; 

	ldx #0
@process_mob_loop:
	lda mob_type, x
	beq @process_next_mob

	phx
	jsr process_mob
	plx

	jsr mob_update_screen_position

@process_next_mob:
	inx
	cpx #MOB_MAX
	bne @process_mob_loop

	; Store the input_state for next time
	lda input_state
	sta last_input_state
	; Clear input state so we can start building it up again
	stz input_state

	;
	; End Process
	;

	bra @main_loop

@exit:
	; Restore IRQ handler
	sei
	lda default_irq_vector
	sta IRQVec
	lda default_irq_vector+1
	sta IRQVec+1
	cli

	; Reset VERA and return to BASIC
	lda #$80
	sta VERA_ctrl
	rts

;
; Capture Input
;

capture_input:
	lda #0
	jsr JOYSTICK_GET

	; When first starting the emulator, the joystate is always $00 until something is pressed,
	; we just return right away if we're in that state.
	cmp #0
	beq @early_return

	; Check if a joystick is present.
	cpy #$ff
	bne @process_joystate
@early_return:
	rts

@process_joystate:
	eor #$ff

	; We're keeping the data in the NES format, so if
	; this is NES (or keyboard) then jump to storing.
	cpx #2
	bcc @store_input

	; Make the SNES button flags match the NES ones.

	; Move the B button from bit 7 to 6
	sta ZP_TMP
	and #%10000000
	lsr
	ora ZP_TMP
	sta ZP_TMP

	; Move the A button from the X register.
	rmb7 ZP_TMP
	txa
	and #%10000000
	ora ZP_TMP

@store_input:
	; Combine with existing input state and store.
	ora input_state
	sta input_state

	rts

;
; Process MOBs
;


; IN: A = MOB type, X = MOB id
; Affects: A, X, Y
process_mob:
	stx mob_current
	dec
	asl
	tax
	lda mob_current
	jmp (process_mob_jump_table, x)

process_mob_jump_table:
	.addr process_mob_player
	.addr process_mob_shot
	.addr process_mob_enemy
	.addr process_mob_explosion

;
; Process Player
;

; IN: A = MOB id
process_mob_player:
	; Set X to MOB id for any sub-routines that are expecting that.
	ldx #0
	
	bbs PlayerState::AIRBORNE, mob_state, @player_state_airborne

@player_state_grounded:
	;
	; Player is on the ground
	;

	bbr PlayerState::SHOOTING, mob_state, @check_jump

	; Count down our shooting counter to return to previous animation.
	dec mob_data1
	bne @check_jump
	lda mob_animation
	cmp #Animation::PLAYER_RUN_SHOOT
	bne :+
	lda #Animation::PLAYER_RUN
	bra :++
:	lda #Animation::PLAYER_IDLE
:	sta mob_animation
	rmb PlayerState::SHOOTING, mob_state

@check_jump:
	; Check to see if they just pressed jump
	bbr InputState::BUTTON_A, input_state, @not_jumping
	bbs InputState::BUTTON_A, last_input_state, @not_jumping

	lda #(PlayerState::AIRBORNE | PlayerState::JUMPING)
	tsb mob_state
	bne @check_movement

	lda #PLAYER_VECTOR_JUMP
	sta mob_vector_y

	lda #Animation::PLAYER_JUMP
	jsr mob_start_animation

	bra @check_movement

@not_jumping:
	; Check to see if there's ground under the player.
	ldy #MobType::PLAYER
	jsr mob_collision_get_bottom_edge
	inc collision_point_A1_y+1
	jsr map_check_collision_h_edge
	cmp #1
	beq @check_movement

	; We're falling!
	smb PlayerState::AIRBORNE, mob_state
	lda #Animation::PLAYER_JUMP
	jsr mob_start_animation

	bra @check_movement

@player_state_airborne:
	;
	; Player is airborne
	;

	; Check to see if we are jumping, or falling
	bbr PlayerState::JUMPING, mob_state, @apply_gravity

	; Check to see our vector is still going up
	lda mob_vector_y
	bpl @jump_is_over

	; Check to see if the user is still pressing the jump button
	bbs InputState::BUTTON_A, input_state, @apply_gravity

	; If not, we want to cancel the jump (ie. stop going up)
	stz mob_vector_y
@jump_is_over:
	rmb PlayerState::JUMPING, mob_state
@apply_gravity:
	clc
	lda mob_vector_y
	adc #GRAVITY_VECTOR_Y
	cmp #GRAVITY_TERMINAL_Y
	bpl @clamp_gravity
	sta mob_vector_y
	bra @done_applying_gravity
@clamp_gravity:
	; Clamp to terminal gravity
	lda #GRAVITY_TERMINAL_Y
	sta mob_vector_y
@done_applying_gravity:
@done_player_state_airborne:

@check_movement:
	;
	; Both airborne and grounded players can move
	;

@check_left:
	bbr InputState::DPAD_LEFT, input_state, @check_right
	lda #PLAYER_VECTOR_LEFT
	sta mob_vector_x

	smb PlayerState::FACING_LEFT, mob_state
	lda #PlayerState::MOVING
	tsb mob_state
	bne @done_checking_movement

	bbs PlayerState::AIRBORNE, mob_state, @done_checking_movement

	lda #Animation::PLAYER_RUN
	jsr mob_start_animation

	bra @done_checking_movement

@check_right:
	bbr InputState::DPAD_RIGHT, input_state, @not_left_or_right
	lda #PLAYER_VECTOR_RIGHT
	sta mob_vector_x

	rmb PlayerState::FACING_LEFT, mob_state
	lda #PlayerState::MOVING
	tsb mob_state
	bne @done_checking_movement

	bbs PlayerState::AIRBORNE, mob_state, @done_checking_movement

	lda #Animation::PLAYER_RUN
	jsr mob_start_animation

	bra @done_checking_movement

@not_left_or_right:
	lda #PlayerState::MOVING
	trb mob_state
	bne @done_checking_movement

	stz mob_vector_x

	lda mob_state
	bit #PlayerState::AIRBORNE
	bne @done_checking_movement
	bit #PlayerState::SHOOTING
	bne :+
	lda #Animation::PLAYER_IDLE
	bra :++
:	lda #Animation::PLAYER_SHOOT
:	jsr mob_start_animation
@done_checking_movement:

@check_shoot:
	bbr InputState::BUTTON_B, input_state, @done_checking_shoot
	bbs InputState::BUTTON_B, last_input_state, @done_checking_shoot

	jsr player_spawn_shot
@done_checking_shoot:

	jsr mob_update_position_with_map_collision

	bbr PlayerState::AIRBORNE, mob_state, @done_handling_collisions

	lda collision_state
	bit #(CollisionState::COLLIDE_DOWN | CollisionState::COLLIDE_UP)
	beq @done_handling_collisions

	; Stop ascending/descending and clear jumping flag
	stz mob_vector_y
	rmb PlayerState::JUMPING, mob_state

	bit #(CollisionState::COLLIDE_DOWN)
	beq @done_handling_collisions

	; Stop falling and update animation
	rmb PlayerState::AIRBORNE, mob_state
	bbs PlayerState::MOVING, mob_state, @start_moving_animation
	lda #Animation::PLAYER_IDLE
	jsr mob_start_animation
	bra @done_handling_collisions
@start_moving_animation:
	lda #Animation::PLAYER_RUN
	jsr mob_start_animation
@done_handling_collisions:

@update_camera:
	clc
	lda mob_position_x_h
	ror
	sta ZP_TMP+1
	lda mob_position_x_l
	ror
	sta ZP_TMP

	ldx #3
:	clc
	lda ZP_TMP+1
	ror
	sta ZP_TMP+1
	lda ZP_TMP
	ror
	sta ZP_TMP
	dex
	bne :-

	lda ZP_TMP+1
	bne @check_camera_x_max
@check_camera_x_min:
	lda ZP_TMP
	cmp #PLAYER_CENTER_X
	bcc @set_camera_x_min
	bra @camera_x_subtract_center
@check_camera_x_max:
	cmp #>(CAMERA_X_LIMIT + PLAYER_CENTER_X)
	bcc @camera_x_subtract_center
	lda ZP_TMP
	cmp #<(CAMERA_X_LIMIT + PLAYER_CENTER_X)
	bcs @set_camera_x_max
@camera_x_subtract_center:
	sec
	lda ZP_TMP
	sbc #PLAYER_CENTER_X
	sta camera_x
	lda ZP_TMP+1
	sbc #0
	sta camera_x+1
	bra @done_updating_camera
@set_camera_x_min:
	stz camera_x
	stz camera_x+1
	bra @done_updating_camera
@set_camera_x_max:
	lda #<CAMERA_X_LIMIT
	sta camera_x
	lda #>CAMERA_X_LIMIT
	sta camera_x+1
@done_updating_camera:

@return:
	rts

;
; Player spawn shot
;
player_spawn_shot:
	phx

	ldx #SHOT_START
:	lda mob_type, x
	beq @found_free_mob_for_shot
	inx
	cpx #(SHOT_START + SHOT_MAX)
	bne :-
	jmp @return

@found_free_mob_for_shot:
	lda #MobType::SHOT
	sta mob_type, x
	lda #Animation::SHOT
	jsr mob_start_animation

	smb PlayerState::SHOOTING, mob_state

	lda mob_animation
	cmp #Animation::PLAYER_IDLE
	beq @use_shoot_animation
	cmp #Animation::PLAYER_RUN
	beq @use_run_shoot_animation
	bra @set_shoot_start_position

@use_shoot_animation:
	lda #Animation::PLAYER_SHOOT
	bra @finish_setting_shoot_animation
@use_run_shoot_animation:
	lda #Animation::PLAYER_RUN_SHOOT
@finish_setting_shoot_animation:
	sta mob_animation

	; Start timer for how long to maintain this animation
	lda #PLAYER_SHOT_JIFFIES
	sta mob_data1

@set_shoot_start_position:
	bbs PlayerState::FACING_LEFT, mob_state, @set_shoot_start_position_left

	clc
	lda mob_position_x_l
	adc #<PLAYER_SHOT_OFFSET_RIGHT_X
	sta mob_position_x_l, x
	lda mob_position_x_h
	adc #>PLAYER_SHOT_OFFSET_RIGHT_X
	sta mob_position_x_h, x

	clc
	lda mob_position_y_l
	adc #<PLAYER_SHOT_OFFSET_RIGHT_Y
	sta mob_position_y_l, x
	lda mob_position_y_h
	adc #>PLAYER_SHOT_OFFSET_RIGHT_Y
	sta mob_position_y_h, x

	bra @done_setting_shoot_start_position
@set_shoot_start_position_left:
	clc
	lda mob_position_x_l
	adc #<PLAYER_SHOT_OFFSET_LEFT_X
	sta mob_position_x_l, x
	lda mob_position_x_h
	adc #>PLAYER_SHOT_OFFSET_LEFT_X
	sta mob_position_x_h, x

	clc
	lda mob_position_y_l
	adc #<PLAYER_SHOT_OFFSET_LEFT_Y
	sta mob_position_y_l, x
	lda mob_position_y_h
	adc #>PLAYER_SHOT_OFFSET_LEFT_Y
	sta mob_position_y_h, x
@done_setting_shoot_start_position:

	; Start the lifetime counter
	lda #SHOT_LIFETIME_JIFFIES
	sta mob_data1, x

	; Set the vector in the right direction
	bbs PlayerState::FACING_LEFT, mob_state, @shot_vector_left
	lda #SHOT_VECTOR_RIGHT
	bra @store_shot_vector
@shot_vector_left:
	lda #SHOT_VECTOR_LEFT
@store_shot_vector:
	sta mob_vector_x, x
	stz mob_vector_y, x
@return:
	plx
	rts

;
; Process Shot
;

; IN: A = MOB id
process_mob_shot:
	tax

	dec mob_data1, x
	bne :+
	jsr mob_clear
	bra @return
:	jsr mob_update_position
@return:
	rts

;
; Process Enemy
;

; IN: A = MOB id
process_mob_enemy:
	rts

;
; Process Explosion
;

; IN: A = MOB id
process_mob_explosion:
	rts

;
; MOB start animation
;

; IN: A = animation ID, X = mob id
; Affects: A, Y
mob_start_animation:
	sta mob_animation, x
	tay
	lda sprite_animation_frame_jiffies_lut, y
	sta mob_frame_jiffies_remaining, x
	stz mob_frame, x
	rts

;
; MOB: update postion based on current vector
;

; IN: X = MOB id
; OUT: A = Y position high-byte
; Affects: A
mob_update_position:

	; Add our signed Q4.4 values to unsigned Q12.4 numbers

	clc
	lda mob_vector_x, x
	adc mob_position_x_l, x
	sta mob_position_x_l, x
	lda mob_vector_x, x
	bmi :+
	lda #0
	bra :++
:	lda #$ff
:	adc mob_position_x_h, x
	sta mob_position_x_h, x

	clc
	lda mob_vector_y, x
	adc mob_position_y_l, x
	sta mob_position_y_l, x
	lda mob_vector_y, x
	bmi :+
	lda #0
	bra :++
:	lda #$ff
:	adc mob_position_y_h, x
	sta mob_position_y_h, x

	rts

; IN: X = MOB id
; Affects: A, Y
mob_update_position_with_map_collision:
	stz collision_state
	
	ldy mob_type, x

	lda mob_vector_x, x
	bmi @move_left
	bne @move_right
	bra @check_y

@move_right:
	clc
	lda mob_vector_x, x
	adc mob_position_x_l, x
	sta mob_position_x_l, x
	lda #0
	adc mob_position_x_h, x
	sta mob_position_x_h, x

	jsr mob_collision_get_right_edge

	jsr map_check_collision_v_edge
	cmp #1
	bne @check_y

	smb CollisionState::COLLIDE_RIGHT, collision_state

	; Calculate X position that isn't colliding
	sec
	lda #$f0 ; All the way to the right (on the previous tile - see the dec below)
	sbc mob_collision_right_l_lut, y
	sta mob_position_x_l, x
	lda collision_point_A1_x+1
	dec      ; Move back to the previous tile
	sbc mob_collision_right_h_lut, Y
	sta mob_position_x_h, x
	bra @check_y

@move_left:
	clc
	lda mob_vector_x, x
	adc mob_position_x_l, x
	sta mob_position_x_l, x
	lda #$ff
	adc mob_position_x_h, x
	sta mob_position_x_h, x

	jsr mob_collision_get_left_edge

	jsr map_check_collision_v_edge
	cmp #1
	bne @check_y

	smb CollisionState::COLLIDE_LEFT, collision_state

	; Calculate X position that isn't colliding
	sec
	lda #$00 ; All the way to the left (on the previous tile - see the inc below)
	sbc mob_collision_left_l_lut, y
	sta mob_position_x_l, x
	lda collision_point_A1_x+1
	inc      ; Move back to the previous tile
	sbc mob_collision_left_h_lut, y
	sta mob_position_x_h, x

@check_y:
	lda mob_vector_y, x
	bmi @move_up
	bne @move_down
	rts

@move_down:
	clc
	lda mob_vector_y, x
	adc mob_position_y_l, x
	sta mob_position_y_l, x
	lda mob_vector_y, x
	lda #0
	adc mob_position_y_h, x
	sta mob_position_y_h, x

	jsr mob_collision_get_bottom_edge

	jsr map_check_collision_h_edge
	cmp #1
	bne @return

	smb CollisionState::COLLIDE_DOWN, collision_state

	; Calculate Y position that isn't colliding
	sec
	lda #$f0 ; All the way to the bottom (on the previous tile - see the dec below)
	sbc mob_collision_bottom_l_lut, y
	sta mob_position_y_l, x
	lda collision_point_A1_y+1
	dec      ; Move back to the previous tile
	sbc mob_collision_bottom_h_lut, y
	sta mob_position_y_h, x
	bra @return

@move_up:
	clc
	lda mob_vector_y, x
	adc mob_position_y_l, x
	sta mob_position_y_l, x
	lda mob_vector_y, x
	lda #$ff
	adc mob_position_y_h, x
	sta mob_position_y_h, x

	jsr mob_collision_get_top_edge

	jsr map_check_collision_h_edge
	cmp #1
	bne @return

	smb CollisionState::COLLIDE_UP, collision_state

	; Calculate Y position that isn't colliding
	sec
	lda #$00 ; All the way to the top (on the previous tile - see the inc below)
	sbc mob_collision_top_l_lut, y
	sta mob_position_y_l, x
	lda collision_point_A1_y+1
	inc      ; Move back to the previous tile
	sbc mob_collision_top_h_lut, y
	sta mob_position_y_h, x

@return:
	rts

;
; MOB: Get edges of a mob's collision box
;

; IN: X = mob, Y = mob type
; OUT: collision_point_A1 (top right) and collision_point_A2 (bottom right)
; Affects: A
mob_collision_get_right_edge:
	; Calculate the x value for the right edge.
	clc
	lda mob_collision_right_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A1_x
	sta collision_point_A2_x
	lda mob_collision_right_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A1_x+1
	sta collision_point_A2_x+1

	; Calculate the top y value.
	clc
	lda mob_collision_top_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A1_y
	lda mob_collision_top_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A1_y+1

	; Calculate the bottom y value.
	clc
	lda mob_collision_bottom_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A2_y
	lda mob_collision_bottom_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A2_y+1

	rts

; IN: X = mob, Y = mob type
; OUT: collision_point_A1 (top left) and collision_point_A2 (bottom left)
; Affects: A
mob_collision_get_left_edge:
	; Calculate the x value for the left edge.
	clc
	lda mob_collision_left_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A1_x
	sta collision_point_A2_x
	lda mob_collision_left_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A1_x+1
	sta collision_point_A2_x+1

	; Calculate the top y value.
	clc
	lda mob_collision_top_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A1_y
	lda mob_collision_top_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A1_y+1

	; Calculate the bottom y value.
	clc
	lda mob_collision_bottom_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A2_y
	lda mob_collision_bottom_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A2_y+1

	rts

; IN: X = mob, Y = mob type
; OUT: collision_point_A1 (bottom left) and collision_point_A2 (bottom right)
; Affects: A
mob_collision_get_bottom_edge:
	; Calculate the y value for the bottom edge.
	clc
	lda mob_collision_bottom_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A1_y
	sta collision_point_A2_y
	lda mob_collision_bottom_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A1_y+1
	sta collision_point_A2_y+1

	; Calculate the left x value.
	clc
	lda mob_collision_left_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A1_x
	lda mob_collision_left_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A1_x+1

	; Calculate the right x value.
	clc
	lda mob_collision_right_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A2_x
	lda mob_collision_right_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A2_x+1

	rts

; IN: X = mob, Y = mob type
; OUT: collision_point_A1 (top left) and collision_point_A2 (top right)
; Affects: A
mob_collision_get_top_edge:
	; Calculate the y value for the top edge.
	clc
	lda mob_collision_top_l_lut, y
	adc mob_position_y_l, x
	sta collision_point_A1_y
	sta collision_point_A2_y
	lda mob_collision_top_h_lut, y
	adc mob_position_y_h, x
	sta collision_point_A1_y+1
	sta collision_point_A2_y+1

	; Calculate the left x value.
	clc
	lda mob_collision_left_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A1_x
	lda mob_collision_left_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A1_x+1

	; Calculate the right x value.
	clc
	lda mob_collision_right_l_lut, y
	adc mob_position_x_l, x
	sta collision_point_A2_x
	lda mob_collision_right_h_lut, y
	adc mob_position_x_h, x
	sta collision_point_A2_x+1

	rts

;
; Map collision: Check if an edge overlaps a tile with collision on the map
;

; IN: collision_point_A1 (top), collision_point_A2 (bottom)
; OUT: A = 1/0 for collision
map_check_collision_v_edge:
	phx
	phy

	ldx collision_point_A1_y+1
	ldy collision_point_A1_x+1
:	jsr map_check_collision
	cmp #1
	beq @return
	inx
	cpx collision_point_A2_y+1
	bcc :-
	beq :-

@return:
	ply
	plx

	rts

; IN: collision_point_A1 (left), collision_point_A2 (right)
; OUT: A = 1/0 for collision
map_check_collision_h_edge:
	phx
	phy

	ldx collision_point_A1_y+1
	ldy collision_point_A1_x+1
:	jsr map_check_collision
	cmp #1
	beq @return
	iny
	cpy collision_point_A2_x+1
	bcc :-
	beq :-

@return:
	ply
	plx

	rts

;
; MOB: update screen position based on world position
;

; IN: X = MOB id
; Affects: A
mob_update_screen_position:
	; Shift the X >> 4
	clc
	lda mob_position_x_h, x
	ror
	sta ZP_TMP+1
	lda mob_position_x_l, x
	ror
	sta ZP_TMP

	ldy #3
:	clc
	lda ZP_TMP+1
	ror
	sta ZP_TMP+1
	lda ZP_TMP
	ror
	sta ZP_TMP
	dey
	bne :-

	; Subtract the camera x
	sec
	lda ZP_TMP
	sbc camera_x
	sta mob_screen_x_l, x
	lda ZP_TMP+1
	sbc camera_x+1
	sta mob_screen_x_h, x
	
	; Shift the Y >> 4
	clc
	lda mob_position_y_h, x
	ror
	sta ZP_TMP+1
	lda mob_position_y_l, x
	ror
	sta ZP_TMP

	ldy #3
:	clc
	lda ZP_TMP+1
	ror
	sta ZP_TMP+1
	lda ZP_TMP
	ror
	sta ZP_TMP
	dey
	bne :-

	; Subtract the camera y
	sec
	lda ZP_TMP
	sbc camera_y
	sta mob_screen_y_l, x
	lda ZP_TMP+1
	sbc camera_y+1
	sta mob_screen_y_h, x
		
	rts

;
; MOB: clear
;

; IN: X = MOB id
mob_clear:
	stz mob_type, x
	stz mob_state, x
	stz mob_vector_x, x
	stz mob_vector_y, x
	stz mob_position_x_l, x
	stz mob_position_x_h, x
	stz mob_position_y_l, x
	stz mob_position_y_h, x
	stz mob_screen_x_l, x
	stz mob_screen_x_h, x
	stz mob_screen_y_l, x
	stz mob_screen_y_h, x
	stz mob_animation, x
	stz mob_frame, x
	stz mob_frame_jiffies_remaining, x
	stz mob_data1, x
	stz mob_data2, x
	stz mob_data3, x
	stz mob_data4, x
	rts

;
; MAP: Check collision
;

; IN: X = tile Y position, Y = tile X position
; OUT: A = 1/0 for collision
map_check_collision:
	phx
	phy

	lda #<map_collision_data
	sta ZP_PTR_1
	lda #>map_collision_data
	sta ZP_PTR_1+1

	cpx #0
	beq @check_data

	; Move our pointer to the correct row
:	clc
	lda ZP_PTR_1
	adc #(MAP_WIDTH >> 3)
	sta ZP_PTR_1
	lda ZP_PTR_1+1
	adc #0
	sta ZP_PTR_1+1
	dex
	bne :-

@check_data:
	tya
	pha

	; Divide by 8 to get the byte index in the collision data, and put in Y.
	lsr
	lsr
	lsr
	tay

	; Multiply by 8, and subtract the original value to get the bit, and put in X.
	asl
	asl
	asl
	sta map_check_collision_temp
	sec
	pla
	sbc map_check_collision_temp
	tax

	; Shift the bit we need over to bit 0.
	lda (ZP_PTR_1), y
	cpx #0
	beq @mask_and_return
:	lsr
	dex
	bne :-

@mask_and_return:
	; Mask out all the other bits
	and #%00000001

	ply
	plx

	rts

map_check_collision_temp: .byte 0

;
; IRQ handler
;

custom_irq_handler:
	;jsr JOYSTICK_SCAN
	;jsr capture_input

	lda VERA_isr
	and #VSYNC_BIT
	beq @return

	; Game tick (aka "process")
	jsr vsync_process_mobs

	; Update layer 1 scroll position
	lda camera_x
	sta VERA_L1_hscroll_l
	lda camera_x+1
	sta VERA_L1_hscroll_h
	lda camera_y
	sta VERA_L1_vscroll_l
	lda camera_y+1
	sta VERA_L1_vscroll_h

	; Update parallaxing layer 0 scroll position (25% speed)
	clc
	lda camera_x+1
	ror
	sta vsync_temp+1
	lda camera_x
	ror
	sta vsync_temp
	clc

	lda vsync_temp+1
	ror
	sta VERA_L0_hscroll_h
	lda vsync_temp
	ror
	sta VERA_L0_hscroll_l

	lda #1
	sta vsync_complete
@return:
	jmp (default_irq_vector)
 
;
; Process MOBs
;

vsync_process_mobs:
	VERA_SET_ADDR VRAM_SPRITE1_sprattr, 1
	ldx #0
@process_mob:
	lda mob_type, x
	beq @skip_nonexstant_mob

	ldy mob_animation, x

	; Update frame index if necessary
	lda mob_frame_jiffies_remaining, x
	beq @check_frame
	dec
	sta mob_frame_jiffies_remaining, x
	bne @check_frame
	; Increment the frame, and reset the jiffies
	inc mob_frame, x
	lda sprite_animation_frames_lut, y
	sta mob_frame_jiffies_remaining, x

@check_frame:
	; Reset to first frame if necessary
	lda mob_frame, x
	cmp sprite_animation_frames_lut, y
	bne @calculate_frame_address
	lda #0
	sta mob_frame, x

@calculate_frame_address:
	phx
	tax

	clc
	lda sprite_animation_base_l_lut, y
	adc sprite_frame_lut, x
	sta VERA_data0
	lda sprite_animation_base_h_lut, y
	adc #0
	sta VERA_data0

	plx

@update_rest_of_sprite_attributes:
	lda mob_screen_x_l, x
	sta VERA_data0
	lda mob_screen_x_h, x
	sta VERA_data0

	lda mob_screen_y_l, x
	sta VERA_data0
	lda mob_screen_y_h, x
	sta VERA_data0

	lda mob_state, x
	and #FACING_LEFT_FLAG
	ora #$0C ; Combine with the correct z-depth
	sta VERA_data0

	lda sprite_animation_config_lut, y
	sta VERA_data0
	bra @process_next_mob

@skip_nonexstant_mob:
	; Clear out the sprite attributes.
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
	stz VERA_data0
@process_next_mob:
	inx
	cpx #MOB_MAX
	beq @return
	jmp @process_mob
@return:
	rts

;
; Big Data Storage
;

map_collision_data: .res 256
