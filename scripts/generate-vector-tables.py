#!/usr/bin/env python3

from math import sqrt

class Vector2(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "(%s, %s)" % (self.x, self.y)
    
    def div(self, v):
        return Vector2(self.x / v, self.y / v)

    def mul(self, v):
        return Vector2(self.x * v, self.y * v)

    def length(self):
        return sqrt(self.x*self.x + self.y*self.y)

    def normalized(self):
        return self.div(self.length())

def print_word_stream(lst):
    hex_list = []
    for x in lst:
        # Effectively bit shift 6 to the left
        x = x * 32
        # Make sure it's a 16-bit integer
        if x > pow(2, 16):
            raise Exception("Number can't fit in a 16-bit value")
        x = int(x) & 0xFFFF
        # Convert to hex and add to the list
        hex_list.append('$' + format(x, 'x').zfill(4))
    print(".word " + ", ".join(hex_list))

def main():
    directions = [
        # Up
        Vector2(0, -1),
        # Up-Right
        Vector2(1, -1),
        # Right
        Vector2(1, 0),
        # Down-Right
        Vector2(1, 1),
        # Down
        Vector2(0, 1),
        # Down-Left
        Vector2(-1, 1),
        # Left
        Vector2(-1, 0),
        # Up-Left
        Vector2(-1, -1),
    ]

    speeds = [60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0]

    for speed in speeds:
        print("%s pixels per second\n" % speed)

        vectors = list(map(lambda x: x.normalized().mul(speed / 60.0), directions))
        print_word_stream(map(lambda v: v.x, vectors))
        print_word_stream(map(lambda v: v.y, vectors))
        print("")

if __name__ == '__main__': main()
