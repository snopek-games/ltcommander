#!/usr/bin/env python3

import sys
import argparse
from PIL import Image

class Palette(object):
    def __init__(self, maximum, use_true_black=False):
        self.colors = [(0, 0, 0)]
        self.max = maximum
        self.use_true_black = use_true_black

    def add_or_lookup(self, color):
        if len(color) == 4 and color[3] != 0xFF:
            return 0

        # Remove lower nibble, converting to a 12-bit color
        color = (color[0] & 0xF0, color[1] & 0xF0, color[2] & 0xF0)

        for index, existing in enumerate(self.colors):
            if index == 0 and self.use_true_black:
                continue
            if color == existing:
                return index

        if len(self.colors) == self.max:
            raise Exception("Can't have more than %s colors in palette" % self.max)

        self.colors.append(color)

        return len(self.colors) - 1
    
    def from_binary(self, input):
        is_first_byte = True
        first_byte = 0
        for byte in input[2:]:
            if is_first_byte:
                first_byte = byte
                is_first_byte = False
            else:
                color = (
                    (byte << 4) & 0xF0,
                    (first_byte & 0xF0),
                    (first_byte << 4) & 0xF0,
                )
                self.add_or_lookup(color)
                is_first_byte = True

    def to_binary(self):
        output = bytearray([0, 0])
        for color in self.colors:
            output.append(color[1] | color[2] >> 4)
            output.append(color[0] >> 4)
        return output

def write_image(output, im, palette, base_x, base_y, tile_width, tile_height):
    for y in range(tile_height):
        last_index = None
        for x in range(tile_width):
            color = im.getpixel((base_x + x, base_y + y))
            index = palette.add_or_lookup(color)

            # @todo configurable bit depths (this is just 4bpp)
            if last_index is not None:
                output.append((last_index << 4) | index)
                last_index = None
            else:
                last_index = index


def main():
    parser = argparse.ArgumentParser(description='Convert tilesets from images to binary data ready to be loaded into VRAM on the Commander x16')
    parser.add_argument('input_filename', help='The image file to convert')
    parser.add_argument('output_filename', help='The binary file to write')
    parser.add_argument('--append', '-A', action='store_true', default=False, help='Append data to binary file if it exists')
    parser.add_argument('--palette', '-p', metavar="FILENAME", help='Use an existing binary palette file')
    parser.add_argument('--generate-palette', '-P', metavar="FILENAME", help='Generates a new binary palette file')
    parser.add_argument('--extend-palette', '-e', action='store_true', default=False, help='Add colors to an existing palette')
    parser.add_argument('--tiled', '-t', metavar="WIDTHxHEIGHT", help='Put into memory as a collection of tiles')
    parser.add_argument('--direction', '-d', metavar="DIRECTION", default='horizontal', help='Whether to tile in the "horizontal" or "vertical" direction')
    parser.add_argument('--use-true-black', action='store_true', default=False, help='If necessary, add another entry to the palette for black rather than transparent')

    # @todo Add a 'tile-direction' argument, so the can be started vertically if that makes sense sometimes
    args = parser.parse_args()

    # @todo Validate that we can't use --palette and --generate-palette together
    # @todo Validate that --extend-palette only makes sense with --palette
    # @todo Validate that --direction is either 'horizontal' or 'vertical'

    im = Image.open(args.input_filename)
    im = im.convert("RGBA")

    # @todo have the ability to generate a palette (4, 16 or 256 colors)
    # @todo palette size should be configurable
    # @todo max colors should depend on the bpp (which should be configurable)
    palette = Palette(16, args.use_true_black)
    if args.palette:
        with open(args.palette, 'rb') as fd:
            # Skip the first two bytes.
            fd.read(2)
            palette.from_binary(bytearray(fd.read()))

    if args.tiled:
        # @todo validate this argument
        (tile_width, tile_height) = tuple(map(lambda x: int(x), args.tiled.split('x')))
    else:
        tile_width = im.width
        tile_height = im.height

    # @todo Check that height/width are a multiple of tile_height/width
    col_max = int(im.width / tile_width)
    row_max = int(im.height / tile_height)

    if args.append:
        with open(args.output_filename, "rb") as fd:
            output = bytearray(fd.read())
    else:
        # Start with empty header
        output = bytearray([0, 0])

    if args.direction == 'vertical':
        for col in range(col_max):
            base_x = col * tile_width
            for row in range(row_max):
                base_y = row * tile_height
                write_image(output, im, palette, base_x, base_y, tile_width, tile_height)
    else:
        for row in range(row_max):
            base_y = row * tile_height
            for col in range(col_max):
                base_x = col * tile_width
                write_image(output, im, palette, base_x, base_y, tile_width, tile_height)

    outf = open(args.output_filename, "wb")
    outf.write(output)
    outf.close()

    if (args.palette and args.extend_palette) or args.generate_palette:
        outp_fn = args.palette if args.palette else args.generate_palette
        outp = open(outp_fn, 'wb')
        outp.write(palette.to_binary())
        outp.close()

if __name__ == "__main__": main()

