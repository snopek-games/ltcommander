#!/usr/bin/env python3

import argparse
import json

FLIPPED_HORIZONTALLY_FLAG = 0x80000000
FLIPPED_VERTICALLY_FLAG   = 0x40000000
FLIPPED_DIAGONALLY_FLAG   = 0x20000000

def generate_collision_data(data):
    # @todo we should really be looking for the first tile layer (there could be object layers in there)
    layer_data = data['layers'][0]['data']

    output = bytearray([0, 0])

    current_byte = 0
    current_bit = 0

    for tile in layer_data:
        # Clear the flags
        tile = tile & ~(FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG)

        if tile != 0:
            # We store the bits backwards, because it's easier to access them
            # this way in assembly.
            current_byte = current_byte | 0x80

        current_bit += 1
        if current_bit == 8:
            output.append(current_byte)
            current_byte = 0
            current_bit = 0
        else:
            current_byte = current_byte >> 1

    if current_byte > 0 or current_bit > 0:
        output.append(current_byte)

    return output

def main():
    parser = argparse.ArgumentParser(description='Generates binary collision data for the given tilemap')
    parser.add_argument('input_filename', help='The JSON file to convert')
    parser.add_argument('output_filename', help='The binary file to write')
    args = parser.parse_args()

    with open(args.input_filename, 'rt') as infile:
        data = json.load(infile)
        with open(args.output_filename, 'wb') as outfile:
            outfile.write(generate_collision_data(data))

if __name__ == "__main__": main()

