#!/usr/bin/env python3

import argparse
import json

FLIPPED_HORIZONTALLY_FLAG = 0x80000000
FLIPPED_VERTICALLY_FLAG   = 0x40000000
FLIPPED_DIAGONALLY_FLAG   = 0x20000000

def generate_map(data, palette_offset, default_tile):
    # @todo we should really be looking for the first tile layer (there could be object layers in there)
    layer_data = data['layers'][0]['data']

    output = bytearray([0, 0])

    for tile in layer_data:
        if tile & FLIPPED_DIAGONALLY_FLAG:
            raise Exception("Tile is flipped diagonally, which isn't supported on the x16")

        # Check for flipping flags
        h_flip = 1 if tile & FLIPPED_HORIZONTALLY_FLAG else 0
        v_flip = 1 if tile & FLIPPED_VERTICALLY_FLAG else 0

        # Clear the flags
        tile = tile & ~(FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG)

        # Tiled starts counting at 1; we need to start at 0
        tile = tile - 1 if tile > 0 else default_tile

        # Split the tile ID into an 8-bit low byte, and 2-bit high byte
        tile_lb = tile & 0xFF
        tile_hb = (tile & 0x300) >> 8 if tile > 0xFF else 0
        #print ("Tile LB: %s" % tile_lb)
        #print ("Tile HB: %s" % tile_hb)

        output.append(tile_lb)
        output.append((palette_offset << 4) | (v_flip << 3) | (h_flip << 2) | tile_hb)

    return output

def main():
    parser = argparse.ArgumentParser(description='Convert tilemaps from Tiled to binary data ready to be loaded into VRAM on the Commander x16')
    parser.add_argument('input_filename', help='The JSON file to convert')
    parser.add_argument('output_filename', help='The binary file to write')
    parser.add_argument('--palette-offset', '-p', type=int, default=0, help='The palette offset for each tile')
    parser.add_argument('--default-tile', '-d', type=int, default=0, help='The default tile to use for empty grid spaces')
    args = parser.parse_args()

    with open(args.input_filename, 'rt') as infile:
        data = json.load(infile)
        with open(args.output_filename, 'wb') as outfile:
            outfile.write(generate_map(data, args.palette_offset, args.default_tile))

if __name__ == "__main__": main()

